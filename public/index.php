<?php
// dati login e autoload generale
require_once('../config/env.php');
require_once('../config/database.php');
require_once('../vendor/autoload.php');
require_once('../config/router.php');
